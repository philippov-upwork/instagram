# Instagram test project

## About

The app allows to search for users and hashtags (https://instagram.com/).

As well as provide info about most liked and commented photos of the user or by given hashtag.

## Installation

1) `git clone git@gitlab.com:philippov-upwork/instagram.git`

2) `bundle`

3) `rails s`

4) `http://localhost:3000`