Rails.application.routes.draw do
  root 'welcome#index'

  get '/search', to: 'welcome#search'

  get '/users/:id', to: 'users#show'
  get '/users/:id/media', to: 'users#media'

  get '/tags/:name', to: 'tags#show'
  get '/tags/:name/media', to: 'tags#media'
end
