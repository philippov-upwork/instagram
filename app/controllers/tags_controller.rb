class TagsController < ApplicationController
  def show
    @tag = InstagramApi.new(params[:name]).call(:tag)
  end

  def media
    from = (Time.zone.now - 1.week).to_i

    render json: InstagramApi.new(from: from, id: params[:name]).call(:tags_media)
  end
end
