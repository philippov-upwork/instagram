class UsersController < ApplicationController
  def show
    @user = InstagramApi.new(params[:id]).call(:user)
  end

  def media
    from = (Time.zone.now - 1.week).to_i

    render json: InstagramApi.new(from: from, id: params[:id]).call(:users_media)
  end
end
