class WelcomeController < ApplicationController
  def index
  end

  def search
    @users = InstagramApi.new(params[:query]).call(:users_search)
    @tags = InstagramApi.new(params[:query]).call(:tags_search)
  end
end
