class InstagramApi
  CLIENT_ID = '37a531dae8a342339a6e95bfc1611ca4'
  API_URL = 'https://api.instagram.com/v1'

  def initialize(params)
    @params = HashWithIndifferentAccess.new(params)
  end

  def call(method)
    method_data = send(method, @params)
    uri = build_uri(method_data[:path], method_data[:params])

    JSON.parse(Net::HTTP.get_response(uri).body)['data']
  end

  private

  def build_uri(path, params)
    uri = URI("#{API_URL}/#{path}")
    uri.query = URI.encode_www_form(params)

    uri
  end

  def users_search(params)
    {
      path: 'users/search',
      params: {
        q: params[:query],
        client_id: CLIENT_ID
      }
    }
  end

  def users_media(params)
    {
      path: "users/#{params[:id]}/media/recent",
      params: {
        client_id: CLIENT_ID,
        min_timestamp: params[:from]
      }
    }
  end

  def tags_media(params)
    {
      path: "tags/#{params[:id]}/media/recent",
      params: {
        client_id: CLIENT_ID,
        min_timestamp: params[:from]
      }
    }
  end

  def tags_search(params)
    {
      path: 'tags/search',
      params: {
        q: params[:query],
        client_id: CLIENT_ID
      }
    }
  end

  def user(params)
    {
      path: "users/#{params[:id]}",
      params: {
        client_id: CLIENT_ID
      }
    }
  end

  def tag(params)
    {
      path: "tags/#{params[:name]}",
      params: {
        client_id: CLIENT_ID
      }
    }
  end
end
