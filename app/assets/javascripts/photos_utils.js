function mostLikedPhotos(userMedia) {
  return _.sortBy(userMedia, function(item) { return item.likes.count }).reverse();
}

function mostCommentedPhotos(userMedia) {
  return _.sortBy(userMedia, function(item) { return item.comments.count }).reverse();
}

function compilePhotos(templateSelector, photos, limit) {
    var compiledTemplate = _.template($(templateSelector).html());
    var photosHTML = '';
    var photos = photos.slice(0, limit - 1);

    _.forEach(photos, function(photo) {
      photosHTML += compiledTemplate(photo);
    });

    return photosHTML;
}
