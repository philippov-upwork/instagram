function getUserMedia(userId) {
  $.get('/users/' + userId + '/media', function(userMedia) {
    $('.js-most-liked-photos').html(
      compilePhotos('#js-user-photo-liked-template', mostLikedPhotos(userMedia), 10)
    );
    $('.js-most-commented-photos').html(
      compilePhotos('#js-user-photo-commented-template', mostCommentedPhotos(userMedia), 10)
    );
  });
}

$(function() {
  if($('.user').length > 0) {
    getUserMedia(location.pathname.split('/').reverse()[0]);
  }
});
