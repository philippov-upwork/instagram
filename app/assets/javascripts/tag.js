function getTagMedia(userId) {
  $.get('/tags/' + userId + '/media', function(userMedia) {
    $('.js-most-liked-photos').html(
      compilePhotos('#js-user-photo-liked-template', mostLikedPhotos(userMedia), 10)
    );
    $('.js-most-commented-photos').html(
      compilePhotos('#js-user-photo-commented-template', mostCommentedPhotos(userMedia), 10)
    );
  });
}

$(function() {
  if($('.tag').length > 0) {
    getTagMedia(location.pathname.split('/').reverse()[0]);
  }
});
